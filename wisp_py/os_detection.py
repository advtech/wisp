#This script is designed to detect what OS WISP is running on currently. 
#TODO: - Add more OS/Hardware Support
#      - Add OS based commands/switchs and/or file to ulitize this script
#      - Add Intel Detection (unable to at the time due to not running an intel based system)
#      - Neaten up code where possibe
# Part of Project WISP, an ANI project
# CHANGELOG: 
# 		- Added method to pass the OS we detected to any other scripts/class
#		- Neatened up log file names to a single term
#		- Added version support for different version of linux
#		- Currently only Ubuntu is defined.
#		- Updated the comments to be the same
import platform
import sys
import os

def os_detected(system_os):
	pi_file = "/proc/device-tree/model"
	platform_name = platform.uname
	linux_file = "/etc/os-release"
	#Check to see if we are running on an UNIX based system
	if platform_name()[0].startswith("Linux") == True:
        #Check to see if we are running on an ARM based Processor or x86_64
		if platform_name()[4].startswith("x86_64") == True:
			#print ("WISP has detected that is running on a Linux based system")
			#return "Linux_x86_64"
			if os.path.isfile(linux_file) and os.access(linux_file, os.R_OK) == True:
				if ((open(linux_file).readline().partition("="))[2].rstrip('\n').replace('"','').startswith("Ubuntu")) == True:
					return "Ubuntu"
				elif ((open(linux_file).readline().partition("="))[2].rstrip('\n').replace('"','').startswith("Ubuntu")) == False:
					return unknownSystem()
        #Checks to see if we are running on Raspberry Pi or an Android
		elif platform_name()[4].startswith("arm") == True:
			if os.path.isfile(pi_file) and os.access(pi_file, os.R_OK) == True:
				if (open(pi_file).readline().startswith("Raspberry")) == True:
							#print ("Wisp is running on a Raspberry Pi. This is not a supported device at this time")
							return "Raspberry Pi"
				elif (open(pi_file).readline().startswith("Raspberry")) == False:
					import android
					#print ("WISP has detected that is running on an Android based system")
					return "Android"
	#Check if we are on a Windows based system                                
	elif platform_name()[0].startswith("Windows")== True:
		if platform_name()[4].startswith("AMD64") == True:
			#print("WISP has detected that is running on a Windows based system running an AMD processor")
			return "Windows_AMD64"
		else:
			return unknownSystem()
pass

def unknownSystem():
	#Otherwise, Dump the System info.
		return("WISP is running on an unidentified system. System called "+ str(platform.uname()[1]) +" detected running " + str(platform.uname()[0]) + " release/version "+ str(platform.uname()[2]) +" on " + str(platform.uname()[4]) + " based processor")
pass
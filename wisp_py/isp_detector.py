#This script is designed to determine the connection provider via an API query
#TODO:
#	Imporve the detection by reducing the amount of reduntant code
# Part of Project Wisp, an ANI
# Changelog
# Added code to check the version of Python and choose the correct method to query the API
import sys, android, os, ssl
def isp_api_enabled(bool):
	return False
pass

python_url =''
python_ip = ''

def pyVersionCheck_url(str):
	if sys.version_info[0] < 3:
		import urllib2
		return python2_url(str)
	else:
		import urllib
		return python3_url(str)
pass


def pyVersionCheck_ip(str):
	if sys.version_info[0] < 3:
		import urllib2
		return python2_ip(str)
	else:
		import urllib
		return python3_ip(str)
pass


url = 'https://api.ipdata.co/?api-key=a6b3529217a8cb1b20b7529cfc5364fd06725aa76bf95c8ee42998c6'
def ssl_unverified():
	if (not os.environ.get('PYTHONHTTPSVERIFY','') and getattr(ssl, '_create_unverified_context', None)):
		ssl._create_default_https_context= ssl._create_unverified_context
pass


def python2_url(str):
	from urllib2 import Request, urlopen
	ssl_unverified()
	headers = { 'Accept': 'application/json' } 
	request = Request(url, headers=headers) 
	carrier = urlopen(request).readlines()[13].rstrip().replace('"','').rstrip(',').partition(":")
	return (carrier)
	
pass

def python2_ip(str):
	from urllib2 import Request, urlopen
	ssl_unverified()
	headers = { 'Accept': 'application/json' } 
	request = Request(url, headers=headers) 
	carrier_ip = urlopen(request).readlines()[1].rstrip().replace('"','').rstrip(',').partition(":")
	return (carrier_ip)
pass

def python3_url(str):
	from urllib import request
	from urllib.request import urlopen
	ssl_unverified()
	headers = {'Accept' :' application/json'}
	info_request = request.Request(url, headers=headers)
	isp_name = urlopen(info_request).readlines()[13].decode('utf-8').rstrip().replace('"','').rstrip(',').partition(":")
	return (isp_name)
pass

def python3_ip(str):
	from urllib import request
	from urllib.request import urlopen
	ssl_unverified()
	headers = {'Accept' :' application/json'}
	info_request = request.Request(url, headers=headers)
	ip_address = urlopen(info_request).readlines()[1].decode('utf-8').rstrip('\n').replace('"','').replace(',','').partition(":")
	return (ip_address)
pass

def isp_info(str):
	isp_api_enabled(True)
	return ("Network provider is" + pyVersionCheck_url(python_url)[2] + " using the following ip: " + pyVersionCheck_ip(python_ip)[2])
pass
#This script allows WISP to send alerts to subcribed devices using the Notify Web API
# Due to security risk, the registered channel is not listed in readable code and may be wiped at any time if needed
# 
# Changelog:
#       - Setup restrictions as to what is allowed commands
#       - Removed the command checker since it didn't pertain to the main function of this script
# This is part of Project WISP, an ANI project
from notify_run import Notify
import os_detection, isp_detector, time, command_tree
from isp_detector import isp_api_enabled, isp_info
notify = Notify()
notify.endpoint =('https://notify.run/QhgVHT42RFir5Ndo')
def askingforCommand(bool):
    return True
pass

def sendText():
    textToBeSend = input("What would you like me to say? ")
    notify.send(textToBeSend)
    time.sleep(1)
    if askingforCommand(True):
        secondCommand = input("Would you like to send another text? ")
        if secondCommand in ['Yes', 'Y', 'y', 'yes','YES']:
            sendText()         
        elif secondCommand in ['No', 'N', 'n', 'no', 'NO']:
            askingforCommand(True)  
            print("Alright, Exiting WISP text mode now. Goodbye" )
            command_tree.user_input("")
        else:
            print("Invalid Option. Exiting")
    return askingforCommand(False)  
pass

def sendDump():
    print("Sending System Dump")
    notify.send(os_detection.unknownSystem())
    time.sleep(1)
    return askingforCommand(True)  
pass

def sendISPInfo():
    print (isp_info(str))
    time.sleep(1)
    if askingforCommand(True):
        sendNotifyCommand = input("Send Data via Notify? ")
        if sendNotifyCommand in ['Yes', 'Y', 'y', 'yes','YES']:
            notify.send(isp_info(str))
            askingforCommand(True)
            print ("Returning to Command interface")
            command_tree.user_input("")
        elif sendNotifyCommand in ['No', 'N', 'n', 'no', 'NO']:
            askingforCommand(True)
            print ("Returning to Command interface")
            command_tree.user_input("")
    time.sleep(1)
    return askingforCommand(True)
pass
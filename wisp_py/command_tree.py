# Command_Tree.py
# Basic choice tree for Project WISP, an ANI Project
# TODO:
#		- Add more commands that reflect new functions added.
#		- Add a Log Dump Option
# Changelog:
#		- Created file to slim send_message and to allow future expansion
from send_message import sendText, sendDump, sendISPInfo
import os_detection

invalid_Command = "Sorry, not a valid option! Exiting now!"
def askingforCommand(bool):
    return True
pass

def user_input(str):
	while askingforCommand(True):
		command = input("Please enter your command: ")
		if command in ['SendText', 'SendTxt', 'Txt', 'Text']:
			return sendText()
		elif command in ['DumpLog', 'Log', 'DumpL']:
			#TODO: Add a log dump option
			print("ERROR: DUMPING OF LOGS IS UNABLE TO BE DONE AT THIS TIME DUE TO THE FEATURE NOT BEING CREATED! PLEASE CREATE THIS FUNCTION!")
			return None
		elif command in ['ISP','isp']:
			return sendISPInfo()
		elif command in ['DataDump','Data', 'DumpD']:
			return sendDump()
		elif command in ['Exit', 'Quit', 'Q']:
			print ("Exiting Wisp Now. Have a good day!")
			return None
		else:
			askingforCommand(False)
			return print(invalid_Command)
pass
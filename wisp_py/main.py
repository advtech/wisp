#This is the main brain of WISP, an ANI project
#TODO:
#		Add more functionality to WISP
#		Reduce the amount of imports to a single file if possible
#		Create a changelog script to allow for better readablity
#Changelog:
#		Version 0.0.01-ALPHA:
#		-	Added OS Detection and ISP Detection to allow WISP to learn about her enviroment
#		Version 0.0.02-ALPHA:
#		-	WISP is now capable of sending messages via Notify.run Web API
#This code belongs to Project WISP, an ANI Project
# Copyrighted to "" and ""

#Dependencies check
def notify_installed(bool):
	return bool
pass
from install_requirements import install
try:
	from notify_run import Notify
	notify_installed(True)
except ImportError:
	print("ERROR: notify_run was not found! Installing now!")
	install('notify_run')
	notify_installed(True)
pass


import os_detection, isp_detector, command_tree
from isp_detector import isp_api_enabled, isp_info
#Variables 
sys_os = ''
version = "0.0.02-ALPHA"

#Actual WISP functions
print ("Loading WISP " + version)
print ("Running on " + os_detection.os_detected(sys_os))
#print ("To prevent API Limits from being reached, ISP_Detector has been disabled")
if isp_api_enabled(bool) == False:
	print("Currently, Network Data is disabled by default to prevent API Overload. You may access it via Commands -> ISP")
	#To enable ISP Query, Remove the comment '#'
else:
	isp_info
if notify_installed(True):
	print ("Able to set info to subbed devices")
else:
	print ("Unable to use notify_run. Is your net broken?")
command_tree.user_input("")
#print ("Current EOF due to features not being added")